package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

type msg2 struct {
}

func (m msg2) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	time.Sleep(time.Second * 2)
	num := r.URL.Query().Get("number")

	nm, err := strconv.Atoi(num)
	if err != nil {
		fmt.Fprintf(w, "error: %s", "sent right number")

		return
	}

	if nm < 2 {
		fmt.Fprintf(w, "error: %s", "sent number biger then 1")

		return
	}

	if nm > 20 {
		fmt.Fprintf(w, "error: %s", "sent number lesser then 21")

		return
	}

	fmt.Fprintf(w, "string: %s number:%d  factorial:%d", num, nm, factorial(nm))
}

func main() {
	msgHandler := msg2{}

	fmt.Println("Server is listening...")
	http.ListenAndServe("localhost:8180", msgHandler)
}

func factorial(n int) int {
	result := 1

	for i := 1; i <= n; i++ {
		result *= i
	}

	return result
}
